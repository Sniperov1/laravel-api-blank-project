<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function index(array $params = [])
    {
        $query = User::query();
        $query = $this->queryApplyFilter($query, $params);
        $query = $this->queryApplyOrderBy($query, $params);
        $query = $this->queryApplyPagination($query, $params);

        return $query->get();
    }

    public function get(array $params = []) : ?User
    {
        $query = User::query();
        $query = $this->queryApplyFilter($query, $params);
        return $query->first();
    }

    protected function queryApplyFilter($query, array $params = [])
    {
        if (isset($params['id'])) {
            $query->where('id', $params['id']);
        }

        if (isset($params['filter'])) {
            $query->where('name', 'like', '%' . $params['filter'] . '%')
                ->orWhere('email', 'like', '%' . $params['filter'] . '%');
        }

        if (isset($params['email'])) {
            $query->where('email', $params['email']);
        }

        if (isset($params['name'])) {
            $query->where('name', $params['name']);
        }

        return $query;
    }

    protected function queryApplyOrderBy($query, $params)
    {
        $desc = 'asc';
        if (isset($params['descending']) && $params['descending'] == 'true') {
            $desc = 'desc';
        }

        if (isset($params['sortBy'])) {
            $query->orderBy($params['sortBy'], $desc);
        }
        return $query;
    }

    protected function queryApplyPagination($query, $params)
    {
        if (isset($params['startRow'])) {
            $query->offset($params['startRow']);
        }
        if (isset($params['rowsPerPage'])) {
            $query->limit($params['rowsPerPage']);
        } else {
            $query->limit(100);
        }
        return $query;
    }
}